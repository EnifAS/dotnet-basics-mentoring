﻿namespace Task1
{
    public class Product
    {
        public Product(string name, double price)
        {
            Name = name;
            Price = price;
        }

        public string Name { get; set; }

        public double Price { get; set; }

        public override bool Equals(object obj)
        {
            if ((obj is null) || (!GetType().Equals(obj.GetType())))
            {
                return false;
            }

            var product = obj as Product;

            return (Name == product.Name) && (Price == product.Price);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Price.GetHashCode();
        }
    }
}
