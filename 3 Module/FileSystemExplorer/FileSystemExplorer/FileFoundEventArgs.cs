﻿using System;

namespace FileSystemExplorer
{
    internal class FileFoundEventArgs : EventArgs
    {
        public string FileName { get; set; }

        public bool AbortSearch { get; set; }

        public bool Exclude { get; set; }
    }
}
