﻿using System;

namespace FileSystemExplorer
{
    internal class FileSystemManager
    {
        public string sourcePath = "C:\\Test";

        public void GetFileSystemsEntities()
        {
            //var fileSystemVisitor = new FileSystemVisitor();
            var fileSystemVisitor = new FileSystemVisitor(
                (string fileName) => fileName.Contains("3"),
                (string folderName) => folderName.Contains("3"));

            fileSystemVisitor.SourceFolderNotFound += () => Console.WriteLine("Sourse folder not found!");
            fileSystemVisitor.FileSystemThrowException += (string message) => Console.WriteLine($"Exception during visiting file system.\n{message}");
            fileSystemVisitor.StartSearch += () => Console.WriteLine("Search started.");
            fileSystemVisitor.EndSearch += () => Console.WriteLine("Search ended.");
            fileSystemVisitor.FileFound += (string fileName) => Console.WriteLine($"Found file: '{fileName}'");
            fileSystemVisitor.DirectoryFound += (string directoryName) => Console.WriteLine($"Found folder: '{directoryName}'");
            
            fileSystemVisitor.FilteredFileFound += (object sender, FileFoundEventArgs fileFoundEventArgs) =>
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"Filtered file found: '{fileFoundEventArgs.FileName}'");
                Console.ResetColor();

                if (fileFoundEventArgs.AbortSearch)
                {
                    Environment.Exit(0);
                }
            };

            fileSystemVisitor.FilteredDirectoryFound += (string directoryName) =>
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"Filtered directory found: '{directoryName}'");
                Console.ResetColor();
            };

            fileSystemVisitor.GetFileSystemsEntities(sourcePath);
        }
    }
}
