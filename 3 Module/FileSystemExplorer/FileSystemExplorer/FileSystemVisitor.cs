﻿using System;
using System.IO;

namespace FileSystemExplorer
{
    public delegate bool FileFilterHandler(string fileName);

    public delegate bool FolderFilterHandler(string folderName);

    internal class FileSystemVisitor
    {
        public event Action SourceFolderNotFound;

        public event Action<string> FileSystemThrowException;

        public event Action StartSearch;

        public event Action EndSearch;

        public event Action<string> FileFound;

        public event Action<string> DirectoryFound;

        public event EventHandler<FileFoundEventArgs> FilteredFileFound;

        public event Action<string> FilteredDirectoryFound;

        private readonly FileFilterHandler _fileFilterHandler;

        private readonly FolderFilterHandler _folderFilterHandler;

        public FileSystemVisitor()
        {
        }

        public FileSystemVisitor(
            FileFilterHandler fileFilterHandler,
            FolderFilterHandler folderFilterHandler)
        {
            _fileFilterHandler = fileFilterHandler;
            _folderFilterHandler = folderFilterHandler;
        }

        public void GetFileSystemsEntities(string path)
        {
            StartSearch?.Invoke();

            try
            {
                TraverseFolderTree(path);
            }
            catch (Exception ex)
            {
                FileSystemThrowException?.Invoke(ex.Message);
            }

            EndSearch?.Invoke();
        }

        public void TraverseFolderTree(string path)
        {
            if (!Directory.Exists(path))
            {
                SourceFolderNotFound?.Invoke();

                return;
            }

            var fileEntries = Directory.GetFiles(path);

            foreach (var file in fileEntries)
            {
                var fileName = file[(file.LastIndexOf("\\") + 1)..];

                FileFound?.Invoke(fileName);

                if (_fileFilterHandler != null && _fileFilterHandler(file))
                {
                    FilteredFileFound?.Invoke(this, new FileFoundEventArgs
                    {
                        FileName = fileName,
                        AbortSearch = true,
                        Exclude = true
                    });
                }
            }

            var subdirectories = Directory.GetDirectories(path);

            foreach (var directory in subdirectories)
            {
                var directoryName = directory[(directory.LastIndexOf("\\") + 1)..];

                DirectoryFound?.Invoke(directoryName);

                if (_folderFilterHandler != null && _folderFilterHandler(directory))
                {
                    FilteredDirectoryFound?.Invoke(directoryName);
                }

                TraverseFolderTree(directory);
            }
        }
    }
}
