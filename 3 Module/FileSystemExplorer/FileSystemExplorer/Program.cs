﻿

namespace FileSystemExplorer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var fileSystemManager = new FileSystemManager();

            fileSystemManager.GetFileSystemsEntities();
        }
    }
}
