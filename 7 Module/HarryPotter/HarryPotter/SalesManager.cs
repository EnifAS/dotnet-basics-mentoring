﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HarryPotter
{
    public class SalesManager
    {
        private const int OneBookPrice = 8;

        private readonly Dictionary<int, double> _sales;

        public SalesManager()
        {
            _sales = new Dictionary<int, double>
            {
                { 1, 1 },
                { 2, 0.95 },
                { 3, 0.9 },
                { 4, 0.8 },
                { 5, 0.75 }
            };
        }

        public double GetTotalCost(IEnumerable<string> books)
        {
            if (books is null)
            {
                throw new ArgumentNullException();
            }

            if (!books.Any())
            {
                throw new ArgumentException();
            }

            var uniqueTitels = books.Distinct().Count();

            var repeatsCount = books.Count() - uniqueTitels;

            return OneBookPrice * uniqueTitels * _sales[uniqueTitels] + OneBookPrice * repeatsCount;
        }
    }
}
