using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace HarryPotter.Tests
{
    public class SalesManagerTests
    {
        private const int OneBookCost = 8;
        private readonly SalesManager _salesManager;

        public SalesManagerTests()
        {
            _salesManager = new SalesManager();
        }

        [Test]
        public void GetTotalCost_BooksCollectionIsNull_ThrowArgumentNullException() =>
            Assert.Throws<ArgumentNullException>(
                () => _salesManager.GetTotalCost(null));

        [Test]
        public void GetTotalCost_BooksCollectionIsEmpty_ThrowArgumentException() =>
            Assert.Throws<ArgumentException>(
                () => _salesManager.GetTotalCost(new List<string>()));

        [Test]
        public void GetTotalCost_OneBook_ReturnCostWithoutDiscount()
        {
            var oneBook = new List<string> { "Harry Potter and Philosopher's Stone" };
            var expectedResult = OneBookCost;

            var totalCost = _salesManager.GetTotalCost(oneBook);

            Assert.AreEqual(expectedResult, totalCost);
        }

        [Test]
        public void GetTotalCost_TwoDifferentBooks_ReturnCostWithFivePercentDiscount()
        {
            var twoBooks = new List<string> 
            { 
                "Harry Potter and Philosopher's Stone",
                "Harry Potter and Chamber of Secrets"
            };

            var expectedResult = OneBookCost * 2 * 0.95;

            var totalCost = _salesManager.GetTotalCost(twoBooks);

            Assert.AreEqual(expectedResult, totalCost);
        }

        [Test]
        public void GetTotalCost_ThreeDifferentBooks_ReturnCostWithTenPercentDiscount()
        {
            var threeBooks = new List<string> 
            { 
                "Harry Potter and Philosopher's Stone",
                "Harry Potter and Chamber of Secrets",
                "Harry Potter and Prisoner of Azkaban"
            };

            var expectedResult = OneBookCost * 3 * 0.9;

            var totalCost = _salesManager.GetTotalCost(threeBooks);

            Assert.AreEqual(expectedResult, totalCost);
        }

        [Test]
        public void GetTotalCost_FourDifferentBooks_ReturnCostWithTwentyPersentDiscount()
        {
            var fourBooks = new List<string> 
            {
                "Harry Potter and Philosopher's Stone",
                "Harry Potter and Chamber of Secrets",
                "Harry Potter and Prisoner of Azkaban",
                "Harry Potter and Goblet of Fire"
            };

            var expectedResult = OneBookCost * 4 * 0.8;

            var totalCost = _salesManager.GetTotalCost(fourBooks);

            Assert.AreEqual(expectedResult, totalCost);
        }

        [Test]
        public void GetTotalCost_FiveDifferentBooks_ReturnCostWithTwentyFivePercentDiscount()
        {
            var fiveBooks = new List<string> 
            { 
                "Harry Potter and Philosopher's Stone",
                "Harry Potter and Chamber of Secrets",
                "Harry Potter and Prisoner of Azkaban",
                "Harry Potter and Goblet of Fire",
                "Harry Potter and Order of the Phoenix"
            };

            var expectedResult = OneBookCost * 5 * 0.75;

            var totalCost = _salesManager.GetTotalCost(fiveBooks);

            Assert.AreEqual(expectedResult, totalCost);
        }

        [Test]
        public void GetTotalCost_TwoIdenticalBooks_ReturnCostWithoutDiscount()
        {
            var twoBooks = new List<string> 
            { 
                "Harry Potter and Philosopher's Stone",
                "Harry Potter and Philosopher's Stone"
            };

            var expectedResult = OneBookCost * 2;

            var totalCost = _salesManager.GetTotalCost(twoBooks);

            Assert.AreEqual(expectedResult, totalCost);
        }

        [Test]
        public void GetTotalCost_ThreeBooksWithTwoIdentical_ReturnCostWithFivePercentDiscountForTwoBooks()
        {
            var threeBooks = new List<string> 
            { 
                "Harry Potter and Philosopher's Stone",
                "Harry Potter and Philosopher's Stone",
                "Harry Potter and Chamber of Secrets",
            };

            var expectedResult = OneBookCost + (OneBookCost * 2 * 0.95);

            var totalCost = _salesManager.GetTotalCost(threeBooks);

            Assert.AreEqual(expectedResult, totalCost);
        }

        [Test]
        public void GetTotalCost_FiveBooksWithThreeIdentical_ReturnCostWithTenPercentDiscountForThreeBooks()
        {
            var fiveBooks = new List<string> 
            {
                "Harry Potter and Philosopher's Stone",
                "Harry Potter and Chamber of Secrets",
                "Harry Potter and Prisoner of Azkaban",
                "Harry Potter and Prisoner of Azkaban",
                "Harry Potter and Prisoner of Azkaban",
            };

            var expectedResult = (OneBookCost * 2) + (OneBookCost * 3 * 0.9);

            var totalCost = _salesManager.GetTotalCost(fiveBooks);

            Assert.AreEqual(expectedResult, totalCost);
        }
    }
}