using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace CalcStats.Tests
{
    public class StatisticsManagerTests
    {
        private readonly StatisticsManager _statisticsManager;

        public StatisticsManagerTests()
        {
            _statisticsManager = new StatisticsManager();
        }

        [Test]
        public void GetSequenceStats_SequenceIsNull_ThrowArgumentNullException() =>
            Assert.Throws<ArgumentNullException>(
                () => _statisticsManager.GetSequenceStats(null));

        [Test]
        public void GetSequenceStats_SequenceIsEmpty_ThrowArgumentException() =>
            Assert.Throws<ArgumentException>(
                () => _statisticsManager.GetSequenceStats(new List<int>()));

        [Test]
        public void GetSequenceStats_OneElement_SameValueForAllStats()
        {
            var oneElementSequence = new List<int> { 1 };
            var expectedResult = new SequenceStatisModel { Average = 1, ElementsCount = 1, Max = 1, Min = 1 };

            var result = _statisticsManager.GetSequenceStats(oneElementSequence);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetSequenceStats_MultipleElements_ValidResult()
        {
            var multipleElementsSequence = new List<int> { 10, -15, 34, 5, 0 };
            var expectedResult = new SequenceStatisModel { Average = 6.8, ElementsCount = 5, Max = 34, Min = -15 };

            var result = _statisticsManager.GetSequenceStats(multipleElementsSequence);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetSequenceStats_NegativeMultipleElements_ValidResult()
        {
            var multipleElementsSequence = new List<int> { -10, -15, -34, -5, -17 };
            var expectedResult = new SequenceStatisModel { Average = -16.2, ElementsCount = 5, Max = -5, Min = -34 };

            var result = _statisticsManager.GetSequenceStats(multipleElementsSequence);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void GetSequenceStats_PositiveMultipleElements_ValidResult()
        {
            var multipleElementsSequence = new List<int> { 10, 1, 654, 9, 43 };
            var expectedResult = new SequenceStatisModel { Average = 143.4, ElementsCount = 5, Max = 654, Min = 1 };

            var result = _statisticsManager.GetSequenceStats(multipleElementsSequence);

            Assert.AreEqual(expectedResult, result);
        }
    }
}