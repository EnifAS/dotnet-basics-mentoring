﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CalcStats
{
    public class StatisticsManager
    {
        public SequenceStatisModel GetSequenceStats(IEnumerable<int> sequence)
        {
            if (sequence is null)
            {
                throw new ArgumentNullException();
            }

            if (!sequence.Any())
            {
                throw new ArgumentException();
            }

            return new SequenceStatisModel
            {
                Max = sequence.Max(),
                Min = sequence.Min(),
                Average = sequence.Average(),
                ElementsCount = sequence.Count()
            };
        }
    }
}
