﻿namespace CalcStats
{
    public class SequenceStatisModel
    {
        public int Min { get; set; }

        public int Max { get; set; }

        public int ElementsCount { get; set; }

        public double Average { get; set; }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                SequenceStatisModel sequenceStatisModel = (SequenceStatisModel)obj;

                return (Max == sequenceStatisModel.Max)
                    && (Min == sequenceStatisModel.Min)
                    && (Average == sequenceStatisModel.Average)
                    && (ElementsCount == sequenceStatisModel.ElementsCount);
            }
        }

        public override int GetHashCode()
        {
            return Min.GetHashCode() 
                ^ Max.GetHashCode() 
                ^ Average.GetHashCode() 
                ^ ElementsCount.GetHashCode();
        }
    }
}
