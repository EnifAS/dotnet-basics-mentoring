using NUnit.Framework;

namespace LeapYear.Tests
{
    public class YearTests
    {
        [TestCase(1600)]
        [TestCase(2000)]
        [TestCase(1988)]
        [TestCase(2132)]
        [TestCase(1764)]
        public void IsLeapYear_LeapYearArgument_ReturnTrue(int year)
        {
            Assert.IsTrue(Year.IsLeapYear(year));
        }

        [TestCase(1700)]
        [TestCase(2200)]
        [TestCase(1997)]
        [TestCase(1774)]
        public void IsLeapYear_NotLeapYearArgument_ReturnFalse(int year)
        {
            Assert.IsFalse(Year.IsLeapYear(year));
        }
    }
}