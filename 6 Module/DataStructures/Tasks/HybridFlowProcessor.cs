﻿using System;
using Tasks.DoNotChange;

namespace Tasks
{
    public class HybridFlowProcessor<T> : IHybridFlowProcessor<T>
    {
        private IDoublyLinkedList<T> _storage;

        public HybridFlowProcessor()
        {
            _storage = new DoublyLinkedList<T>();
        }

        public T Dequeue()
        {
            if (_storage.Length == 0)
            {
                throw new InvalidOperationException("Queue is empty.");
            }

            return _storage.RemoveAt(0);
        }

        public void Enqueue(T item)
        {
            _storage.Add(item);
        }

        public T Pop()
        {
            if (_storage.Length == 0)
            {
                throw new InvalidOperationException("Stack is empty.");
            }

            return _storage.RemoveAt(0);
        }

        public void Push(T item)
        {
            _storage.AddAt(0, item);
        }
    }
}
