﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tasks.DoNotChange;

namespace Tasks
{
    public class DoublyLinkedList<T> : IDoublyLinkedList<T>
    {
        public int Length { get; private set; }

        public Node<T> _head;

        public DoublyLinkedList()
        {
            Length = 0;
        }

        public void Add(T e)
        {
            var node = new Node<T>(e);

            if (_head is null)
            {
                _head = node;
                Length++;
                return;
            }

            var lastNode = _head;

            while (lastNode.Next != null)
            {
                lastNode = lastNode.Next;
            }

            lastNode.Next = node;
            node.Previous = lastNode;
            Length++;
        }

        public void AddAt(int index, T e)
        {
            if (Length < index || index < 0)
            {
                throw new ArgumentOutOfRangeException("DoublyLinkedList is empty.");
            }

            var node = new Node<T>(e);

            var currentIndex = 0;
            var nodeInCurrentIndex = _head;
            var previousNode = (Node<T>)null;

            while (currentIndex < index)
            {
                previousNode = nodeInCurrentIndex;
                nodeInCurrentIndex = nodeInCurrentIndex.Next;
                currentIndex++;
            }

            if (previousNode is null)
            {
                _head = node;
                node.Next = nodeInCurrentIndex;

                Length++;

                return;
            }

            previousNode.Next = node;
            node.Previous = previousNode;
            node.Next = nodeInCurrentIndex;

            if (nodeInCurrentIndex != null)
            {
                nodeInCurrentIndex.Previous = node;
            }

            Length++;
        }

        public T ElementAt(int index)
        {
            if (Length <= index || index < 0)
            {
                throw new IndexOutOfRangeException("DoublyLinkedList is empty.");
            }

            var currentIndex = 0;
            var nodeInCurrentIndex = _head;

            while (currentIndex != index)
            {
                nodeInCurrentIndex = nodeInCurrentIndex.Next;
                currentIndex++;
            }

            return nodeInCurrentIndex.Data;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new LinkedListEnumerator<T>(_head, Length);
        }

        public void Remove(T item)
        {
            if (Length == 0)
            {
                return;
            }

            var currentNode = _head;

            while (currentNode.Next != null && !currentNode.Data.Equals(item))
            {
                currentNode = currentNode.Next;
            }

            var previousNode = currentNode.Previous;
            var nextNode = currentNode.Next;

            if (currentNode.Data.Equals(item))
            {
                if (previousNode != null)
                {
                    previousNode.Next = nextNode;
                }

                if (nextNode != null)
                {
                    nextNode.Previous = previousNode;
                }

                Length--;
            }

            if (previousNode is null)
            {
                _head = nextNode;
            }
        }

        public T RemoveAt(int index)
        {
            if (index < 0 || Length <= index)
            {
                throw new IndexOutOfRangeException("DoublyLinkedList is empty.");
            }

            var currentElement = _head;

            if (index == 0)
            {
                _head = currentElement.Next;

                if (_head != null)
                {
                    _head.Previous = null;
                }

                Length--;

                return currentElement.Data;
            }

            var currentIndex = 0;

            while (currentIndex != index)
            {
                currentElement = currentElement.Next;
                currentIndex++;
            }

            if (currentElement.Previous != null)
            {
                currentElement.Previous.Next = currentElement.Next;
            }

            if (currentElement.Next != null)
            {
                currentElement.Next.Previous = currentElement.Previous;
            }

            Length--;

            return currentElement.Data;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new LinkedListEnumerator<T>(_head, Length);
        }
    }

    class LinkedListEnumerator<T> : IEnumerator<T>
    {
        private Node<T> _head;
        private int _length;
        private Node<T> _currentElement = null;
        private bool _started;

        public LinkedListEnumerator(Node<T> head, int length)
        {
            _head = head;
            _length = length;
        }

        public T Current { get => _currentElement.Data; }

        object IEnumerator.Current { get => _currentElement.Data; }

        public void Dispose()
        {
            _head = null;
            _currentElement = null;
        }

        public bool MoveNext()
        {
            if (!_started)
            {
                _currentElement = _head;
                _started = true;
            }
            else
            {
                _currentElement = _currentElement.Next;
            }

            return _currentElement != null;
        }

        public void Reset()
        {
            _currentElement = _head;
        }
    }
}
