﻿using System;

namespace Task1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            PrintsFirstCharacter();
        }

        private static void PrintsFirstCharacter()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Please, enter a string.");

                    var sourceString = Console.ReadLine();

                    Console.ForegroundColor = ConsoleColor.DarkMagenta;

                    Console.WriteLine("The first charecter of the string is " + $"'{sourceString[0]}'");

                    Console.ResetColor();
                }
                catch (IndexOutOfRangeException)
                { 
                    Console.ResetColor();

                    Console.WriteLine("\nString can't be empty.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception occured: {ex.Message}");
                }
            }
        }
    }
}