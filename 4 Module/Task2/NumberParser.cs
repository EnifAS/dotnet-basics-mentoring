﻿using System;
using System.Collections.Generic;

namespace Task2
{
    public class NumberParser : INumberParser
    {
        private readonly Dictionary<char, int> _validCharacters = new Dictionary<char, int>
        {
            { '-', -1 },
            { '+', 1 },
        };

        private readonly Dictionary<char, int> _validDigit = new Dictionary<char, int>
        {
            { '0', 0 },
            { '1', 1 },
            { '2', 2 },
            { '3', 3 },
            { '4', 4 },
            { '5', 5 },
            { '6', 6 },
            { '7', 7 },
            { '8', 8 },
            { '9', 9 },
        };

        public int Parse(string stringValue)
        {
            var sourceString = stringValue?.Trim();

            if (sourceString is null)
            {
                throw new ArgumentNullException("Input string doesn't contain any digits.");
            }

            if (sourceString.Length == 0)
            {
                throw new FormatException("Input string is empty.");
            }

            long digit = 0;

            var rank = 10;

            var power = 0;

            for (int i = sourceString.Length - 1; i >= 0; i--, power++)
            {
                var symbol = sourceString[i];

                if (i == 0 && _validCharacters.ContainsKey(symbol))
                {
                    digit *= _validCharacters[symbol];

                    if (digit > int.MaxValue || digit < int.MinValue)
                    {
                        throw new OverflowException("The number cannot fit in an Int32.");
                    }

                    return (int)digit;
                }

                if (!_validDigit.ContainsKey(symbol))
                {
                    throw new FormatException("Input string is not a sequence of digits.");
                }

                digit += _validDigit[symbol] * (long)Math.Pow(rank, power);

                if (digit < int.MinValue || (digit > int.MaxValue && i == 0))
                {
                    throw new OverflowException("The number cannot fit in an Int32.");
                }
            }

            return (int)digit;
        }
    }
}