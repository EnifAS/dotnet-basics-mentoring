﻿using System;

namespace Task3.Exceptions
{
    internal class NegativeUserIdException : Exception
    {
        public NegativeUserIdException()
        {
        }

        public NegativeUserIdException(string message)
            : base(message)
        {
        }

        public NegativeUserIdException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
