﻿using System;

namespace Task3.Exceptions
{
    internal class DuplicateTaskException : Exception
    {
        public DuplicateTaskException()
        {
        }

        public DuplicateTaskException(string message)
            : base(message)
        {
        }

        public DuplicateTaskException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
