﻿using System;

namespace Task3.Exceptions
{
    internal class NullUserException : Exception
    {
        public NullUserException()
        {
        }

        public NullUserException(string message)
            : base(message)
        {
        }

        public NullUserException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
