﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task1.DoNotChange;

namespace Task1
{
    public static class LinqTask
    {
        public static IEnumerable<Customer> Linq1(IEnumerable<Customer> customers, decimal limit)
        {
            return customers.Where(customer => customer.Orders.Count() > limit);
        }

        public static IEnumerable<(Customer customer, IEnumerable<Supplier> suppliers)> Linq2(
            IEnumerable<Customer> customers,
            IEnumerable<Supplier> suppliers
        )
        {
            return customers
                .Select(customer => (customer, suppliers
                .Where(supplier => supplier.Country == customer.Country && supplier.City == customer.City)));
        }

        public static IEnumerable<(Customer customer, IEnumerable<Supplier> suppliers)> Linq2UsingGroup(
            IEnumerable<Customer> customers,
            IEnumerable<Supplier> suppliers
        )
        {
            var groupedSuppliers = suppliers.GroupBy(supplier => new { Country = supplier.Country, City = supplier.City });

            return customers
                .Select(customer => (customer, groupedSuppliers
                .Where(w => w.Key.Country == customer.Country && w.Key.City == customer.City)
                .SelectMany(s => s)));
        }

        public static IEnumerable<Customer> Linq3(IEnumerable<Customer> customers, decimal limit)
        {
            return customers.Where(customer => customer.Orders.Any(order => order.Total > limit));
        }

        public static IEnumerable<(Customer customer, DateTime dateOfEntry)> Linq4(
            IEnumerable<Customer> customers
        )
        {
            return customers.Where(customer => customer.Orders.Any())
                .Select(customer => (customer, customer.Orders.Min(order => order.OrderDate)));
        }

        public static IEnumerable<(Customer customer, DateTime dateOfEntry)> Linq5(
            IEnumerable<Customer> customers
        )
        {
            return customers.Where(customer => customer.Orders.Any())
                .Select(customer => (customer, customer.Orders.Min(order => order.OrderDate)))
                .OrderBy(date => date.Item2.Year)
                .ThenBy(date => date.Item2.Month)
                .ThenByDescending(customer => customer.customer.Orders.Sum(order => order.Total))
                .ThenBy(customer => customer.customer.CompanyName);
        }

        public static IEnumerable<Customer> Linq6(IEnumerable<Customer> customers)
        {
            return customers.Where(customer => !customer.PostalCode.All(char.IsDigit)
                                            || customer.Region is null
                                            || !customer.Phone.Any(character => character == '(' || character == ')'));
        }

        public static IEnumerable<Linq7CategoryGroup> Linq7(IEnumerable<Product> products)
        {
            /* example of Linq7result

             category - Beverages
	            UnitsInStock - 39
		            price - 18.0000
		            price - 19.0000
	            UnitsInStock - 17
		            price - 18.0000
		            price - 19.0000
             */

            return products.GroupBy(product => product.Category)
                .Select(category => new Linq7CategoryGroup
                {
                    Category = category.Key,
                    UnitsInStockGroup = category
                        .GroupBy(product => product.UnitsInStock)
                        .Select(unit => new Linq7UnitsInStockGroup 
                        {
                            UnitsInStock = unit.Key, Prices = unit.Select(s => s.UnitPrice).OrderBy(s => s).ToList() 
                        }
                        )
                });
        }

        public static IEnumerable<(decimal category, IEnumerable<Product> products)> Linq8(
            IEnumerable<Product> products,
            decimal cheap,
            decimal middle,
            decimal expensive
        )
        {
            var cp = products.Where(product => product.UnitPrice <= cheap).GroupBy(p => cheap).ToList();
            var mp = products.Where(product => product.UnitPrice > cheap && product.UnitPrice <= middle).GroupBy(p => middle).ToList();
            var ep = products.Where(product => product.UnitPrice > middle && product.UnitPrice <= expensive).GroupBy(p => expensive).ToList();

            var conc = cp.Concat(mp).Concat(ep);

            return conc.Select(s => (s.Key, s.AsEnumerable()));
        }

        public static IEnumerable<(string city, int averageIncome, int averageIntensity)> Linq9(
            IEnumerable<Customer> customers
        )
        {
            throw new NotImplementedException();
        }

        public static string Linq10(IEnumerable<Supplier> suppliers)
        {
            throw new NotImplementedException();
        }
    }
}