﻿using AdoFundamentals.Entities;
using AdoFundamentals.Models;
using AdoFundamentals.Repositories;

namespace AdoFundamentalsTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var connectionString = "Data Source=localhost;Initial Catalog=EStore;Integrated Security=True;TrustServerCertificate=True";

            var productsRepository = new SqlProductsRepository(connectionString);

            var productId = productsRepository.InsertProduct(new Product
            {
                Name = "Desk",
                Description = "Desk description",
                Weight = 30,
                Height = 120.5M,
                Length = 150,
                Width = 50
            });

            productsRepository.UpdateProduct(new Product
            {
                Id = productId,
                Name = "Desk",
                Description = "Desk test description",
                Weight = 30,
                Height = 120.5M,
                Length = 150,
                Width = 50
            });

            var product = productsRepository.GetProduct(productId);
            var productsList = productsRepository.GetProductsList();
            productsRepository.DeletePtoduct(productId);

            var ordersRepository = new SqlOrdersRepository(connectionString);

            var orderId = ordersRepository.InsertOrder(new Order
            {
                Status = "Loading",
                ProductId = 9,
            });

            ordersRepository.UpdateOrder(new Order
            {
                Id = orderId,
                Status = "InProgress",
                ProductId = 9,
            });

            var order = ordersRepository.GetOrder(orderId);

            var ordersList = ordersRepository.SearchOrdersByCriteria(new SearchOrderModel
            {
                Status = "Loading"
            });

            ordersRepository.DeleteOrder(orderId);

            ordersRepository.DeleteOrderByProductId(9);

            ordersRepository.DeleteOrdersByCriteria(new SearchOrderModel
            {
                Month = 1,
                Status = "Done",
            });
        }
    }
}
