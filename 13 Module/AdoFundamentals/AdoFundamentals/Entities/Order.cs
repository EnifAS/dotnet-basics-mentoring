﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdoFundamentals.Entities
{
    public class Order
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Indicate the status of order.")]
        [MaxLength(10, ErrorMessage = "Status name cannot be longer than 50 charecters.")]
        public string Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Required]
        public int ProductId { get; set; }
    }
}
