﻿using System.ComponentModel.DataAnnotations;

namespace AdoFundamentals.Entities
{
    public class Product
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Indicate the name of product.")]
        [MaxLength(50, ErrorMessage = "Name cannot be longer than 50 charecters.")]
        public string Name { get; set; }

        [MaxLength(200, ErrorMessage = "Description cannot be longer than 200 charecters.")]
        public string Description { get; set; }

        [Required]
        public decimal Weight { get; set; }

        [Required]
        public decimal Height { get; set; }

        [Required]
        public decimal Width { get; set; }

        [Required]
        public decimal Length { get; set; }
    }
}
