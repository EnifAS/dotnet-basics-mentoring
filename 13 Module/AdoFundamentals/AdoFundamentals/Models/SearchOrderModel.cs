﻿using System.ComponentModel.DataAnnotations;

namespace AdoFundamentals.Models
{
    public class SearchOrderModel
    {
        public int? Month { get; set; }

        public int? Year { get; set; }

        [MaxLength(10, ErrorMessage = "Status name cannot be longer than 50 charecters.")]
        public string Status { get; set; }

        [MaxLength(50, ErrorMessage = "Name cannot be longer than 50 charecters.")]
        public string ProductName { get; set; }
    }
}
