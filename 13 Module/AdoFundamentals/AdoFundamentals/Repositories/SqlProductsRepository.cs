﻿using AdoFundamentals.Entities;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace AdoFundamentals.Repositories
{
    public class SqlProductsRepository : SqlBaseRepository
    {
        private const string IdColumn = "Id";
        private const string NameColumn = "Name";
        private const string DescriptionColumn = "Description";
        private const string WeightColumn = "Weight";
        private const string HeightColumn = "Height";
        private const string WidthColumn = "Width";
        private const string LengthColumn = "Length";

        public SqlProductsRepository(string connectionString) : base(connectionString) { }

        public int InsertProduct(Product product)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "INSERT Products OUTPUT INSERTED.Id VALUES (@Name, @Description, @Weight, @Height, @Width, @Length)";

            var command = new SqlCommand(query, connection);

            command.Parameters.Add(GetInputStringParameter("Name", product.Name));
            command.Parameters.Add(GetInputStringParameter("Description", product.Description));
            command.Parameters.Add(GetInputDecimalParameter("Weight", product.Weight));
            command.Parameters.Add(GetInputDecimalParameter("Height", product.Height));
            command.Parameters.Add(GetInputDecimalParameter("Width", product.Width));
            command.Parameters.Add(GetInputDecimalParameter("Length", product.Length));

            try
            {
                return (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw new Exception($"The insert of product \"{product.Name}\" wasn't successfull.", ex);
            }
        }

        public Product GetProduct(int productId)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "SELECT Id, Name, Description, Weight, Height, Width, Length FROM Products WHERE Id = @Id";

            var command = new SqlCommand(query, connection);
            command.Parameters.Add(GetInputIntParameter("Id", productId));

            Product product = null;

            using var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    object id = reader[IdColumn];
                    object name = reader[NameColumn];
                    object description = reader[DescriptionColumn];
                    object weight = reader[WeightColumn];
                    object height = reader[HeightColumn];
                    object width = reader[WidthColumn];
                    object length = reader[LengthColumn];

                    product = new Product
                    {
                        Id = (int)id,
                        Name = name.ToString(),
                        Description = description.ToString(),
                        Weight = (decimal)weight,
                        Height = (decimal)height,
                        Width = (decimal)width,
                        Length = (decimal)length,
                    };
                }
            }

            return product;
        }

        public IReadOnlyList<Product> GetProductsList()
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "SELECT Id, Name, Description, Weight, Height, Width, Length FROM Products";

            var command = new SqlCommand(query, connection);

            List<Product> products = null;

            using var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                products = new List<Product>();

                while (reader.Read())
                {
                    object id = reader[IdColumn];
                    object name = reader[NameColumn];
                    object description = reader[DescriptionColumn];
                    object weight = reader[WeightColumn];
                    object height = reader[HeightColumn];
                    object width = reader[WidthColumn];
                    object length = reader[LengthColumn];

                    products.Add(new Product
                    {
                        Id = (int)id,
                        Name = name.ToString(),
                        Description = description.ToString(),
                        Weight = (decimal)weight,
                        Height = (decimal)height,
                        Width = (decimal)width,
                        Length = (decimal)length,
                    });
                }
            }

            return products;
        }

        public void UpdateProduct(Product product)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "UPDATE Products SET Name = @Name, Description = @Description, Weight = @Weight, Height = @Height, Width = @Width, Length = @Length WHERE Id = @Id";

            var command = new SqlCommand(query, connection);

            command.Parameters.Add(GetInputIntParameter("Id", product.Id));
            command.Parameters.Add(GetInputStringParameter("Name", product.Name));
            command.Parameters.Add(GetInputStringParameter("Description", product.Description));
            command.Parameters.Add(GetInputDecimalParameter("Weight", product.Weight));
            command.Parameters.Add(GetInputDecimalParameter("Height", product.Height));
            command.Parameters.Add(GetInputDecimalParameter("Width", product.Width));
            command.Parameters.Add(GetInputDecimalParameter("Length", product.Length));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception($"The update of product \"{product.Name}\" wasn't successfull.", ex);
            }
        }

        public void DeletePtoduct(int productId)
        {
            var ordersRepository = new SqlOrdersRepository(_connectionString);

            ordersRepository.DeleteOrderByProductId(productId);

            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "DELETE FROM Products WHERE Id = @Id";

            var command = new SqlCommand(query, connection);
            command.Parameters.Add(GetInputIntParameter("Id", productId));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception($"The deletion of product wasn't successfull.", ex);
            }
        }
    }
}
