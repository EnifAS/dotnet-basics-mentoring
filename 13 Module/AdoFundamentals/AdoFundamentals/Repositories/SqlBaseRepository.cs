﻿using Microsoft.Data.SqlClient;
using System;
using System.Data;

namespace AdoFundamentals.Repositories
{
    public class SqlBaseRepository
    {
        protected readonly string _connectionString;

        public SqlBaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected SqlParameter GetInputStringParameter(string parameterName, object parameterValue) =>
            new()
            {
                DbType = DbType.String,
                ParameterName = parameterName,
                Value = parameterValue ?? DBNull.Value,
                Direction = ParameterDirection.Input,
            };

        protected SqlParameter GetInputDateTimePatameter(string parameterName, object parameterValue) =>
            new()
            {
                DbType = DbType.DateTime,
                ParameterName = parameterName,
                Value = parameterValue,
                Direction = ParameterDirection.Input,
            };

        protected SqlParameter GetInputDecimalParameter(string parameterName, object parameterValue) =>
            new()
            {
                DbType = DbType.Decimal,
                ParameterName = parameterName,
                Value = parameterValue,
                Direction = ParameterDirection.Input,
            };

        protected SqlParameter GetInputIntParameter(string parameterName, object parameterValue) =>
            new()
            {
                DbType = DbType.Int32,
                ParameterName = parameterName,
                Value = parameterValue,
                Direction = ParameterDirection.Input,
            };
    }
}
