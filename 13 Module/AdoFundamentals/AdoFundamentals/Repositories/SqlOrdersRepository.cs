﻿using AdoFundamentals.Entities;
using AdoFundamentals.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace AdoFundamentals.Repositories
{
    public class SqlOrdersRepository : SqlBaseRepository
    {
        private const string IdColumn = "Id";
        private const string StatusColumn = "Status";
        private const string CreatedDateColumn = "CreatedDate";
        private const string UpdatedDateColumn = "UpdatedDate";
        private const string ProductIdColumn = "ProductId";

        public SqlOrdersRepository(string connectionString) : base(connectionString) { }

        public int InsertOrder(Order order)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "INSERT Orders OUTPUT INSERTED.Id VALUES (@Status, @CreatedDate, @UpdatedDate, @ProductId)";

            var command = new SqlCommand(query, connection);

            command.Parameters.Add(GetInputStringParameter("Status", order.Status));
            command.Parameters.Add(GetInputDateTimePatameter("CreatedDate", DateTime.Now));
            command.Parameters.Add(GetInputDateTimePatameter("UpdatedDate", DateTime.Now));
            command.Parameters.Add(GetInputIntParameter("ProductId", order.ProductId));

            try
            {
                return (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw new Exception($"The insert of order wasn't successfull.", ex);
            }
        }

        public Order GetOrder(int orderId)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "SELECT Id, Status, CreatedDate, UpdatedDate, ProductId FROM Orders WHERE Id = @Id";

            var command = new SqlCommand(query, connection);
            command.Parameters.Add(GetInputIntParameter("Id", orderId));

            Order order = null;

            using var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    object id = reader[IdColumn];
                    object status = reader[StatusColumn];
                    object createdDate = reader[CreatedDateColumn];
                    object updatedDate = reader[UpdatedDateColumn];
                    object productId = reader[ProductIdColumn];

                    order = new Order
                    {
                        Id = (int)id,
                        Status = status.ToString(),
                        CreatedDate = (DateTime)createdDate,
                        UpdatedDate = (DateTime)updatedDate,
                        ProductId = (int)productId,
                    };
                }
            }

            return order;
        }

        public void UpdateOrder(Order order)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "UPDATE Orders SET Status = @Status, UpdatedDate = @UpdatedDate, ProductId = @ProductId WHERE Id = @Id";

            var command = new SqlCommand(query, connection);

            command.Parameters.Add(GetInputIntParameter("Id", order.Id));
            command.Parameters.Add(GetInputStringParameter("Status", order.Status));
            command.Parameters.Add(GetInputDateTimePatameter("UpdatedDate", DateTime.Now));
            command.Parameters.Add(GetInputIntParameter("ProductId", order.ProductId));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception($"The update of order wasn't successfull.", ex);
            }
        }

        public void DeleteOrder(int orderId)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "DELETE FROM Orders WHERE Id = @Id";

            var command = new SqlCommand(query, connection);
            command.Parameters.Add(GetInputIntParameter("Id", orderId));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception($"The deletion of order by ID wasn't successfull.", ex);
            }
        }

        public void DeleteOrderByProductId(int productId)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var query = "DELETE FROM Orders WHERE ProductId = @ProductId";

            var command = new SqlCommand(query, connection);
            command.Parameters.Add(GetInputIntParameter("ProductId", productId));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception($"The deletion of order by product ID wasn't successfull.", ex);
            }
        }

        public IReadOnlyList<Order> SearchOrdersByCriteria(SearchOrderModel searchOrderModel)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var command = new SqlCommand("SearchOrders", connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            command.Parameters.Add(GetInputIntParameter("Month", searchOrderModel.Month));
            command.Parameters.Add(GetInputIntParameter("Year", searchOrderModel.Year));
            command.Parameters.Add(GetInputStringParameter("Status", searchOrderModel.Status));
            command.Parameters.Add(GetInputStringParameter("ProductName", searchOrderModel.ProductName));

            using var reader = command.ExecuteReader();

            var orders = new List<Order>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    object id = reader[IdColumn];
                    object status = reader[StatusColumn];
                    object createdDate = reader[CreatedDateColumn];
                    object updatedDate = reader[UpdatedDateColumn];
                    object productId = reader[ProductIdColumn];

                    orders.Add(new Order
                    {
                        Id = (int)id,
                        Status = status.ToString(),
                        CreatedDate = (DateTime)createdDate,
                        UpdatedDate = (DateTime)updatedDate,
                        ProductId = (int)productId,
                    });
                }
            }

            return orders;
        }

        public void DeleteOrdersByCriteria(SearchOrderModel searchOrderModel)
        {
            using var connection = new SqlConnection(_connectionString);
            connection.Open();

            var command = new SqlCommand("DeleteOrders", connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            command.Parameters.Add(GetInputIntParameter("Month", searchOrderModel.Month));
            command.Parameters.Add(GetInputIntParameter("Year", searchOrderModel.Year));
            command.Parameters.Add(GetInputStringParameter("Status", searchOrderModel.Status));
            command.Parameters.Add(GetInputStringParameter("ProductName", searchOrderModel.ProductName));

            command.ExecuteNonQuery();
        }
    }
}
