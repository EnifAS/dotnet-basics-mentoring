﻿-- Seed  Persons table
SET IDENTITY_INSERT dbo.Persons ON;  

GO

INSERT INTO dbo.Persons (Id, FirstName, LastName)
VALUES (1, 'Homer', 'Simpson');

GO

INSERT INTO dbo.Persons (Id, FirstName, LastName)
VALUES (2, 'Bart', 'Simpson');

GO

INSERT INTO dbo.Persons (Id, FirstName, LastName)
VALUES (3, 'Peter', 'Griffin');

GO

INSERT INTO dbo.Persons (Id, FirstName, LastName)
VALUES (4, 'Rick', 'Sanczes');

GO

INSERT INTO dbo.Persons (Id, FirstName, LastName)
VALUES (5, 'Bender', 'Rodrigues');

GO

SET IDENTITY_INSERT dbo.Persons OFF;

GO

-- Seed Addresses table

SET IDENTITY_INSERT dbo.Addresses ON;

GO

INSERT INTO dbo.Addresses (Id, Street, City, State, ZipCode)
VALUES (1, 'Kuprevicha', 'Minsk', NULL, '210210');

GO

INSERT INTO dbo.Addresses (Id, Street, City, State, ZipCode)
VALUES (2, 'Oliva', 'Gdansk', 'Pomorze', '80-482');

GO

INSERT INTO dbo.Addresses (Id, Street, City, State, ZipCode)
VALUES (3, 'Oakland', 'Orange', 'Florida', '04-88-22');

GO

INSERT INTO dbo.Addresses (Id, Street, City, State, ZipCode)
VALUES (4, 'Stacy Rd.', 'Allen', 'Texes', '728-930387');

GO

INSERT INTO dbo.Addresses (Id, Street, City, State, ZipCode)
VALUES (5, 'Staten Island', 'New York', 'New York', '746920-003');

GO

INSERT INTO dbo.Addresses (Id, Street, City, State, ZipCode)
VALUES (6, 'Clawson', 'Croydon', 'Nevada', '95-3-28');

GO

INSERT INTO dbo.Addresses (Id, Street, City, State, ZipCode)
VALUES (7, '109th Ave', 'Seminole', 'Florida', '33777');

GO

INSERT INTO dbo.Addresses (Id, Street, City, State, ZipCode)
VALUES (8, 'Elliott Ave Yonkers', 'New York', 'New York', '10705');

GO

SET IDENTITY_INSERT dbo.Addresses OFF;

GO

-- Seed Companies table

INSERT INTO dbo.Companies (Name, AddressId)
VALUES ('EPAM', 1);

GO
INSERT INTO dbo.Companies (Name, AddressId)
VALUES ('NowoTECK', 2);

GO

INSERT INTO dbo.Companies (Name, AddressId)
VALUES ('Willson&Co.', 3);

GO

-- Seed Employees table

INSERT INTO dbo.Employees(AddressId, PersonId, CompanyName, Position, EmployeeName)
VALUES (4, 1, 'EPAM', 'Software engineer', 'Homer Simpson');

GO

INSERT INTO dbo.Employees(AddressId, PersonId, CompanyName, Position, EmployeeName)
VALUES (5, 2, 'EPAM', 'Business analyst', 'Bart Simpson');

GO


INSERT INTO dbo.Employees(AddressId, PersonId, CompanyName, Position, EmployeeName)
VALUES (6, 3, 'NowoTECK', 'Director', 'Peter Griffin');

GO

INSERT INTO dbo.Employees(AddressId, PersonId, CompanyName, Position, EmployeeName)
VALUES (7, 4, 'Willson&Co.', 'Lead software engineer', 'Rick Sanchez');

GO

INSERT INTO dbo.Employees(AddressId, PersonId, CompanyName, Position, EmployeeName)
VALUES (8, 5, 'Willson&Co.', 'Senior software engineer', 'Bender Rodrigues');

GO

