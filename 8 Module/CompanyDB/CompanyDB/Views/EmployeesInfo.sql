﻿CREATE VIEW [dbo].[EmployeesInfo]
	AS
	SELECT
	Employees.Id AS EmployeeId,
	CASE 
		WHEN Employees.EmployeeName IS NOT NULL
		THEN Employees.EmployeeName 
		ELSE concat(Persons.FirstName, ' ', Persons.LastName)
	END AS EmployeeFullName,
	concat(Addresses.ZipCode, '_', Addresses.State, ', ', Addresses.City, '-', Addresses.Street) AS EmployeeFullAddress,
	concat(Employees.CompanyName, '(', Employees.Position, ')') AS EmployeeCompanyInfo
FROM dbo.Employees 
	JOIN
     dbo.Addresses ON dbo.Addresses.Id = dbo.Employees.AddressId
	JOIN
     dbo.Persons   ON dbo.Employees.PersonId = dbo.Persons.Id
