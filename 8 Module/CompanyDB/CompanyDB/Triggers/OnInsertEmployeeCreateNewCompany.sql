﻿CREATE TRIGGER OnInsertEmployeeCreateNewCompany
   ON  dbo.Employees 
   AFTER INSERT
AS 
BEGIN
    DECLARE @AddressId INT
	DECLARE @CompanyName NVARCHAR(20)

	SELECT @AddressId = ins.AddressId FROM INSERTED ins
	SELECT @CompanyName = ins.CompanyName FROM INSERTED ins

	INSERT INTO Companies (Name, AddressId)
	VALUES (@CompanyName, @AddressId)
END
GO
