﻿CREATE TABLE [dbo].[Employees]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[AddressId] INT NOT NULL,
	[PersonId] INT NOT NULL,
	[CompanyName] NVARCHAR(20) NULL,
	[Position] NVARCHAR(30) NULL,
	[EmployeeName] NVARCHAR(100) NULL,
    CONSTRAINT [FK_EmployeesToAddresses] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Addresses]([Id]),
	CONSTRAINT [FK_EmployeesToPersons] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Persons]([Id]),
)
