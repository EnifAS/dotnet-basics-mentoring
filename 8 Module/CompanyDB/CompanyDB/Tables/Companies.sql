﻿CREATE TABLE [dbo].[Companies]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(20) NOT NULL,
	[AddressId] INT NOT NULL,
	CONSTRAINT [FK_CompaniesToAddresses] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Addresses]([Id])
)
