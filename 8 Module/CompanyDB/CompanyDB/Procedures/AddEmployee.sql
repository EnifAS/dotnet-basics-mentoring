﻿CREATE PROCEDURE AddEmployee
	@EmployeeName nvarchar(100) = NULL,
	@FirstName nvarchar(50)		= 'UNKNOWN',
	@LastName nvarchar(50)		= 'UNKNOWN',
	@CompanyName nvarchar(50),
	@Position nvarchar(30)		= NULL,
	@Street nvarchar(50),
	@City nvarchar(20)			= NULL,
	@State nvarchar(50)			= NULL,
	@ZipCode nvarchar(50)		= NULL
AS
BEGIN
	SET @EmployeeName = TRIM(@EmployeeName)
	SET @FirstName = TRIM(@FirstName)
	SET @LastName = TRIM(@LastName)

	IF ((@EmployeeName IS NULL OR @EmployeeName = '')
		AND
		(@FirstName IS NULL OR @FirstName = '')
		AND
		(@LastName IS NULL OR @LastName = ''))
	THROW 51000, 'At least one of EmployeeName, FirstName or LastName should not be NULL or empty', 1
		
	SET @CompanyName = SUBSTRING(@CompanyName, 0, 20)

	DECLARE @InsertedPerson table (Id INT)
	DECLARE @InsertedAddress table (Id INT)

	DECLARE @InsertedPersonId INT
	DECLARE @InsertedAdressId INT

	INSERT INTO Persons (FirstName, LastName)
		OUTPUT INSERTED.Id INTO @InsertedPerson
	VALUES (@FirstName, @LastName)

	SELECT @InsertedPersonId = (SELECT Id FROM @InsertedPerson)

	INSERT INTO Addresses (State, City, Street, ZipCode)
		OUTPUT INSERTED.Id INTO @InsertedAddress
	VALUES (@State, @City, @Street, @ZipCode)

	SELECT @InsertedAdressId = (SELECT Id FROM @InsertedAddress)

	INSERT INTO Employees (AddressId, PersonId, CompanyName, Position, EmployeeName)
	VALUES (@InsertedAdressId, @InsertedPersonId, @CompanyName, @Position, @EmployeeName)
END
GO