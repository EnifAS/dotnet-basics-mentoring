﻿using System;
using System.IO;
using System.Reflection;
using System.Text.Json;

namespace FileConfigurationProvider
{
    public class FileConfigurationProvider
    {
        private const string ConfigFileName = "settings.json";

        private readonly string _pathToSettings;

        public FileConfigurationProvider()
        {
            _pathToSettings = GetPathToSettingsFile();
        }

        public void SaveSettings<TObject>(string propertyName, object value) where TObject : new()
        {
            if (!File.Exists(_pathToSettings))
            {
                File.Create(_pathToSettings);
            }

            var streamReader = new StreamReader(_pathToSettings);

            var settings = streamReader.ReadToEnd();

            var deserializedSettings = JsonSerializer.Deserialize<TObject>(settings,
                new JsonSerializerOptions 
                { 
                    NumberHandling = System.Text.Json.Serialization.JsonNumberHandling.AllowReadingFromString 
                });

            var type = deserializedSettings.GetType();

            var requiredProperty = type.GetProperty(propertyName);

            if (requiredProperty is not null)
            {
                requiredProperty.SetValue(deserializedSettings, value);
            }
            else
            {
                throw new ArgumentException($"Settings property {propertyName} doesn't exist.");
            }

            streamReader.Close();

            using var streamWriter = new StreamWriter(_pathToSettings);

            var serializedSettings = JsonSerializer.Serialize(deserializedSettings);

            if (!File.Exists(_pathToSettings))
            {
                File.Create(_pathToSettings);
            }

            streamWriter.Write(serializedSettings);

            streamWriter.Flush();
        }

        public TResult LoadSettings<TObject, TResult>(string propertyName)
            where TObject : new()
        {
            if (!File.Exists(_pathToSettings))
            {
                throw new DirectoryNotFoundException("Settings file does not exists.");
            }

            using var streamReader = new StreamReader(_pathToSettings);

            var settings = streamReader.ReadToEnd();

            var deserializedSettings = JsonSerializer.Deserialize<TObject>(settings,
                new JsonSerializerOptions 
                {
                    NumberHandling = System.Text.Json.Serialization.JsonNumberHandling.AllowReadingFromString
                });

            var type = deserializedSettings.GetType();

            var property = type.GetProperty(propertyName);

            if (property is not null)
            {
                return (TResult)property.GetValue(deserializedSettings);
            }

            throw new ArgumentException($"Settings property {propertyName} doesn't exist.");
        }

        private string GetPathToSettingsFile()
        {
            var rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            return Path.Combine(rootPath, ConfigFileName);
        }
    }
}
