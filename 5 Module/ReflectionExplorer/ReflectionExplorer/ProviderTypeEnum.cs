﻿namespace ReflectionExplorer
{
    internal enum ProviderTypeEnum
    {
        AppSettingsConfigProvider = 0,
        FileConfigurationProvider = 1,
    }
}
