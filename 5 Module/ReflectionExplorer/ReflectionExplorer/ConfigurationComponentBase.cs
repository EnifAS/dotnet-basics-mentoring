﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ReflectionExplorer
{
    public abstract class ConfigurationComponentBase
    {
        private const string ExtentionsFolderName = "extensions";

        private readonly IReadOnlyList<string> _extensionsFiles;

        public ConfigurationComponentBase()
        {
            _extensionsFiles = GetExtensionsFiles();
        }

        public void SaveSettings()
        {
            var properties = this.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attribute = property.GetCustomAttribute(typeof(ConfigurationItemAttribute), false);

                if (attribute is not null && attribute is ConfigurationItemAttribute configurationItemAttribute)
                {
                    var settingName = configurationItemAttribute.SettingName;
                    var providerType = configurationItemAttribute.ProviderType;

                    foreach (var file in _extensionsFiles)
                    {
                        if (Path.GetFileName(file) != $"{providerType}.dll")
                        {
                            continue;
                        }

                        var exportedType = GetExportedType(file, providerType.ToString());

                        var loadMethod = exportedType.GetMethod("SaveSettings");

                        var typeInstance = Activator.CreateInstance(exportedType);

                        var genericLoadMethod = loadMethod.MakeGenericMethod(this.GetType());

                        var propertyValue = property.GetValue(this);

                        genericLoadMethod.Invoke(typeInstance, new[] { settingName, propertyValue });
                    }
                }
            }
        }

        public T GetSettings<T>() where T : new()
        {
            var resultObject = new T();

            var properties = resultObject.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attribute = property.GetCustomAttribute(typeof(ConfigurationItemAttribute), false);

                if (attribute is not null && attribute is ConfigurationItemAttribute configurationItemAttribute)
                {
                    var settingName = configurationItemAttribute.SettingName;
                    var providerType = configurationItemAttribute.ProviderType;

                    foreach (var file in _extensionsFiles)
                    {
                        if (Path.GetFileName(file) != $"{providerType}.dll")
                        {
                            continue;
                        }

                        var exportedType = GetExportedType(file, providerType.ToString());

                        var loadMethod = exportedType.GetMethod("LoadSettings");

                        var typeInstance = Activator.CreateInstance(exportedType);

                        var genericLoadMethod = loadMethod.MakeGenericMethod(typeof(T), property.PropertyType);

                        var propertyValue = genericLoadMethod.Invoke(typeInstance, new[] { settingName });

                        property.SetValue(resultObject, propertyValue);
                    }
                }
            }

            return resultObject;
        }

        private Type GetExportedType(string filePath, string providerType)
        {
            try
            {
                var assembly = Assembly.LoadFile(filePath);

                var assemblyName = assembly.GetName().Name;

                return assembly.GetType($"{assemblyName}.{providerType}");
            }
            catch (Exception)
            {
                throw new Exception("Cannot retrieve exported types.");
            }
        }

        private IReadOnlyList<string> GetExtensionsFiles()
        {
            try
            {
                var rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                var extensionsPath = Path.Combine(rootPath, ExtentionsFolderName);

                return Directory.GetFiles(extensionsPath).ToList();
            }
            catch (Exception)
            {
                throw new Exception("Cannot retrieve application extensions. Please verify path exists and extension files are in folder.");
            }
        }
    }
}
