﻿using System;
using System.Threading.Tasks;

namespace ReflectionExplorer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var user = new User();

            user = user.GetSettings<User>();

            user.UserID = 999;
            user.FirstName = "Jack";
            user.LastName = "Morrison";
            user.BirthDate = new DateTime(1980, 4, 15);

            user.SaveSettings();
        }
    }
}
