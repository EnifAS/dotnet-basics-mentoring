﻿using System;

namespace ReflectionExplorer
{
    internal class User : ConfigurationComponentBase
    {
        public User() : base()
        {
        }

        [ConfigurationItem("UserID", ProviderTypeEnum.AppSettingsConfigProvider)]
        public int UserID { get; set; }

        [ConfigurationItem("FirstName", ProviderTypeEnum.FileConfigurationProvider)]
        public string FirstName { get; set; }

        [ConfigurationItem("LastName", ProviderTypeEnum.FileConfigurationProvider)]
        public string LastName { get; set; }

        [ConfigurationItem("BirthDate", ProviderTypeEnum.AppSettingsConfigProvider)]
        public DateTime BirthDate { get; set; }
    }
}
