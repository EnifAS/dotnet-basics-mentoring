﻿using System;

namespace ReflectionExplorer
{
    [AttributeUsage(AttributeTargets.Property)]
    internal class ConfigurationItemAttribute : Attribute
    {
        public string SettingName { get; set; }

        public ProviderTypeEnum ProviderType { get; set; }

        public ConfigurationItemAttribute(string settingName, ProviderTypeEnum providerType)
        {
            SettingName = settingName;
            ProviderType = providerType;
        }
    }
}
