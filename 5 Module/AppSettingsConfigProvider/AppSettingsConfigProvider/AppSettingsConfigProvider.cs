﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Text.Json;

namespace AppSettingsConfigProvider
{
    public class AppSettingsConfigProvider
    {
        private readonly IConfiguration _configuration;

        public AppSettingsConfigProvider()
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true);

            _configuration = configurationBuilder.Build();
        }

        public void SaveSettings<TObject>(string propertyName, object value)
        {
            var settings = _configuration.GetSection("Settings");

            settings[propertyName] = value.ToString();

            var appSettingsModel = new AppSettingsModel<TObject>();

            _configuration.Bind(appSettingsModel);

            var appSettingsString = JsonSerializer.Serialize(appSettingsModel);

            try
            {
                using var streamWriter = new StreamWriter("appsettings.json");

                streamWriter.Write(appSettingsString);

                streamWriter.Flush();
            }
            catch (Exception)
            {
                throw new Exception("Cannot rewrite 'appsettings.json' file.");
            }
        }

        public TResult LoadSettings<TObject, TResult>(string propertyName) where TResult : IConvertible
        {
            var settings = _configuration.GetSection("Settings");

            var propertyValue = settings[propertyName];

            return propertyValue is null
                ? throw new ArgumentException($"Settings property {propertyName} doesn't exist.")
                : (TResult)Convert.ChangeType(propertyValue, typeof(TResult));
        }

        class AppSettingsModel<T>
        {
            public T Settings { get; set; }
        }
    }
}
