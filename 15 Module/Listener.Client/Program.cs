﻿using System.Net;

namespace Listener.Client
{
    public class Program
    {
        public static async Task Main()
        {
            await CallMyNameMethodAsync();
            //await CallStatusMethodAsync("Information");
            await CallStatusMethodAsync("Success");
            await CallStatusMethodAsync("Redirection");
            await CallStatusMethodAsync("ClientError");
            await CallStatusMethodAsync("ServerError");
            await CallMyNameByHeader();
            await CallMyNameByCookies();
            Console.ReadLine();
        }

        static async Task CallMyNameMethodAsync()
        {
            var httpClient = new HttpClient();
            var responseMessage = await httpClient.GetAsync(@"http://localhost:8888/MyName");
            var content = await responseMessage.Content.ReadAsStringAsync();

            Console.WriteLine("*** The result of calling /MyName method ***");
            Console.WriteLine(content);
            Console.WriteLine("***");
        }

        static async Task CallStatusMethodAsync(string status)
        {
            var httpClient = new HttpClient();
            var responseMessage = await httpClient.GetAsync($"http://localhost:8888/{status}");

            Console.WriteLine($"*** The result of calling /{status} method ***");
            Console.WriteLine($"Status code is {(int)responseMessage.StatusCode}");
            Console.WriteLine("***");
        }

        static async Task CallMyNameByHeader()
        {
            var httpClient = new HttpClient();
            var responseMessage = await httpClient.GetAsync($"http://localhost:8888/MyNameByHeader");
            responseMessage.Headers.TryGetValues("X-MyName", out var nameHeaders);

            Console.WriteLine($"*** The result of calling /MyNameByHeader method ***");
            Console.WriteLine($"Name from X-MyName header is {nameHeaders?.First()}");
            Console.WriteLine("***");
        }

        static async Task CallMyNameByCookies()
        {
            var cookieContainer = new CookieContainer();

            var handler = new HttpClientHandler
            {
                CookieContainer = cookieContainer,
                UseCookies = true,
            };

            var httpClient = new HttpClient(handler);
            await httpClient.GetAsync($"http://localhost:8888/MyNameByCookies");
            Console.WriteLine($"*** The result of calling /MyNameByCookies method ***");
            var responseCookies = cookieContainer.GetCookies(new Uri($"http://localhost:8888/MyNameByCookies"));

            foreach (Cookie cookie in responseCookies)
            {
                string cookieName = cookie.Name;
                string cookieValue = cookie.Value;

                Console.WriteLine($"Cookie from reponse: Name = {cookieName}, Value = {cookieValue}");
            }

            Console.WriteLine("***");
        }
    }
}