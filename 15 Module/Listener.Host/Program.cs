﻿using System.Net;

namespace Listener.Host
{
    class Program
    {
        private static async Task Main()
        {
            ListenIncomingConnections();

            ListenConsoleInput();
        }

        static Task ListenIncomingConnections()
        {
            return Task.Run(() =>
            {
                var listener = new HttpListener();
                listener.Prefixes.Add("http://localhost:8888/");
                listener.Start();

                Console.WriteLine("*** Listening incoming connections on 'http://localhost:8888/'");
                Console.WriteLine("*** Enter 'c' to exit ***");

                while (true)
                {
                    HttpListenerContext context = listener.GetContext();
                    HttpListenerRequest request = context.Request;

                    switch(request.RawUrl)
                    {
                        case "/MyName": ReturnNameInBody(context.Response);
                            break;

                        case "/Information": ReturnInformationStatusCode(context.Response);
                            break;

                        case "/Success": ReturnSuccessStatusCode(context.Response);
                            break;

                        case "/Redirection": ReturnRedirectionStatusCode(context.Response);
                            break;

                        case "/ClientError": ReturnClientErrorStatusCode(context.Response);
                            break;

                        case "/ServerError": ReturnServerErrorStatusCode(context.Response);
                            break;

                        case "/MyNameByHeader": ReturnNameInHeader(context.Response);
                            break;

                        case "/MyNameByCookies": ReturnNameInCookies(context.Response);
                            break;
                    }
                }
            });
        }

        static void ListenConsoleInput()
        {
            while (true)
            {
                var ch = Console.ReadKey().KeyChar;

                if (ch == 'c' || ch == 'C')
                {
                    Environment.Exit(1);
                }
            }
        }

        static void ReturnNameInBody(HttpListenerResponse response)
        {
            var responseString = $"My name is Nastassia.";
            var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);

            response.ContentLength64 = buffer.Length;
            var output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();
        }

        static void ReturnInformationStatusCode(HttpListenerResponse response)
        {
            response.StatusCode = 100;
            response.StatusDescription = "Information";

            response.ContentLength64 = 1;
            var output = response.OutputStream;
            output.WriteByte(0);
            output.Close();
        }

        static void ReturnSuccessStatusCode(HttpListenerResponse response)
        {
            response.StatusCode = 200;
            response.StatusDescription = "Success";

            response.ContentLength64 = 1;
            var output = response.OutputStream;
            output.WriteByte(0);
            output.Close();
        }

        static void ReturnRedirectionStatusCode(HttpListenerResponse response)
        {
            response.StatusCode = 300;
            response.StatusDescription = "Redirection";

            response.ContentLength64 = 1;
            var output = response.OutputStream;
            output.WriteByte(0);
            output.Close();
        }

        static void ReturnClientErrorStatusCode(HttpListenerResponse response)
        {
            response.StatusCode = 400;
            response.StatusDescription = "ClientError";

            response.ContentLength64 = 1;
            var output = response.OutputStream;
            output.WriteByte(0);
            output.Close();
        }

        static void ReturnServerErrorStatusCode(HttpListenerResponse response)
        {
            response.StatusCode = 500;
            response.StatusDescription = "ServerError";

            response.ContentLength64 = 1;
            var output = response.OutputStream;
            output.WriteByte(0);
            output.Close();
        }

        static void ReturnNameInHeader(HttpListenerResponse response)
        {
            response.Headers.Add("X-MyName", "Nastassia");

            response.ContentLength64 = 1;
            var output = response.OutputStream;
            output.WriteByte(0);
            output.Close();
        }

        static void ReturnNameInCookies(HttpListenerResponse response)
        {
            response.Cookies.Add(new Cookie { Name = "MyName", Value = "Nastassia" });

            response.ContentLength64 = 1;
            var output = response.OutputStream;
            output.WriteByte(0);
            output.Close();
        }
    }
}

