﻿using FileCabinetSoftware.Library.Publications;
using System;
using System.Collections;
using System.Collections.Generic;

namespace FileCabinetSoftware.ConsoleUI
{
    public class CardsReader
    {
        public void ShowCardsInformation<T>(IReadOnlyList<T> cards) where T : BasePublication
        {
            foreach (var card in cards)
            {
                var properties = card.GetType().GetProperties();

                foreach (var property in properties)
                {
                    Console.Write($"{property.Name}: ");

                    if(property.PropertyType.IsGenericType 
                        && property.PropertyType.GetGenericTypeDefinition() == typeof(IList<>))
                    {
                        var value = property.GetValue(card) as IList;

                        DisplayCollectionItems(value);
                    }
                    else
                    {
                        Console.WriteLine($"{property.GetValue(card)}");
                    }
                }

                Console.WriteLine();
                Console.WriteLine("==================================");
            }
        }

        private void DisplayCollectionItems(IList value)
        {
            foreach (var item in value)
            {
                foreach (var prop in item.GetType().GetProperties())
                {
                    Console.Write(prop.GetValue(item) + " ");
                }
            }

            Console.WriteLine();
        }
    }
}
