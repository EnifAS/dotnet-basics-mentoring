﻿using System;
using System.Collections.Generic;
using FileCabinetSoftware.Library;
using FileCabinetSoftware.Library.Publications;

namespace FileCabinetSoftware.ConsoleUI
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var storage = new FileSystemStorage();

            var cardsReader = new CardsReader();

            var book = new Book
            {
                ISBN = "978-3-16-148410-0",
                Authors = new List<Author>
                {
                    new Author { FirstName = "Joanne", LastName = "Rowling" }
                },
                Title = "Harry Potter and the Goblet of Fire. Illustrated Edition",
                DatePublished = new DateTime(2000, 7, 8),
                NumberOfPages = 636,
                Publisher = "Bloomsbury",
            }; 

            storage.AddCard(book);

            var result1 = storage.SearchCard("44");
            cardsReader.ShowCardsInformation(result1);
            var result2 = storage.SearchCard("44");
            cardsReader.ShowCardsInformation(result2);
        }
    }
}
