﻿using System;
using System.Text.Json.Serialization;

namespace FileCabinetSoftware.Library.Publications
{
    public abstract class BasePublication
    {
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string Id { get; init; }

        public string Title { get; set; }

        public DateTime DatePublished { get; set; }

        public BasePublication()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
