﻿using System.Collections.Generic;

namespace FileCabinetSoftware.Library.Publications
{
    public class Book : BasePublication
    {
        public string ISBN { get; set; }

        public IList<Author> Authors { get; set; }

        public int NumberOfPages { get; set; }

        public string Publisher { get; set; }
    }
}
