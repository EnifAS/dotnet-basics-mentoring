﻿using System;
using System.Collections.Generic;

namespace FileCabinetSoftware.Library.Publications
{
    public class Patent : BasePublication
    {
        public IList<Author> Authors { get; set; }

        public DateTime ExpirationDate { get; set; }

        public int UniqueId { get; set; }
    }
}
