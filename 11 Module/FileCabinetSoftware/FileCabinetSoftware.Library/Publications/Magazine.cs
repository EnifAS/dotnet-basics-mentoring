﻿namespace FileCabinetSoftware.Library.Publications
{
    public class Magazine : BasePublication
    {
        public int ReleaseNumber { get; set; }
    }
}
