﻿using FileCabinetSoftware.Library.Publications;
using System.Collections.Generic;

namespace FileCabinetSoftware.Library
{
    public interface IStorage
    {
        void AddCard<T>(T publication) where T : BasePublication;

        IReadOnlyList<BasePublication> SearchCard(string cardNumber);
    }
}
