﻿using FileCabinetSoftware.Library.Publications;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text.Json;

namespace FileCabinetSoftware.Library
{
    public class FileSystemStorage : IStorage
    {
        private const string FileCabinetName = "FileCabinet";
        private readonly string _pathToFolder;
        private readonly MemoryCache _memoryCache;

        public FileSystemStorage()
        {
            var currentDirectory = Environment.CurrentDirectory;
            _pathToFolder = Path.Combine(currentDirectory, FileCabinetName);

            _memoryCache = new MemoryCache("fileSystemCache");
        }

        public void AddCard<T>(T publication) where T : BasePublication
        {
            if (!Directory.Exists(_pathToFolder))
            {
                Directory.CreateDirectory(_pathToFolder);
            }

            var fileName = $"{publication.GetType().Name}_#{publication.Id}.json";
            var filePath = Path.Combine(_pathToFolder, fileName);

            using var fileStream = new FileStream(filePath, FileMode.Create);

            JsonSerializer.SerializeAsync(fileStream, publication);
        }

        public IReadOnlyList<BasePublication> SearchCard(string cardNumber)
        {
            var directoryInfo = new DirectoryInfo(_pathToFolder);
            var foundFiles = directoryInfo.GetFiles($"*{cardNumber}*.json");

            var cards = new List<BasePublication>();

            foreach (var file in foundFiles)
            {
                if (_memoryCache.Contains(file.Name))
                {
                    cards.Add((BasePublication)_memoryCache.Get(file.Name));
                }
                else
                {
                    var fileStream = file.OpenRead();
                    using var reader = new StreamReader(fileStream);

                    var fileTypeName = file.Name.Split('_').First();
                    var fileType = Type.GetType($"FileCabinetSoftware.Library.Publications.{fileTypeName}");

                    var fileData = reader.ReadToEnd();
                    var deserializedFile = JsonSerializer.Deserialize(fileData, fileType);

                    cards.Add((BasePublication)deserializedFile);

                    var cacheItem = new CacheItem(file.Name, deserializedFile);
                    var expirationTime = GetExpirationTime(fileTypeName);
                    var cacheItemPolicy = new CacheItemPolicy
                    {
                        AbsoluteExpiration = expirationTime
                    };

                    _memoryCache.Add(cacheItem, cacheItemPolicy);
                }
            }

            return cards;
        }

        private DateTimeOffset GetExpirationTime(string fileTypeName) => fileTypeName switch
        {
            "Book" => DateTimeOffset.Now.AddSeconds(120),
            "LocalizedBook" => DateTimeOffset.Now.AddSeconds(60),
            "Magazine" => DateTimeOffset.Now.AddSeconds(0),
            "Patent" => DateTimeOffset.Now.AddSeconds(30),
            _ => throw new NotImplementedException(),
        };
    }
}
