﻿namespace DapperFundamentals
{
    public enum Statuses
    {
        NotStarted,
        Loading,
        InProgress,
        Arrived,
        Unloading, 
        Cancelled, 
        Done
    }
}
