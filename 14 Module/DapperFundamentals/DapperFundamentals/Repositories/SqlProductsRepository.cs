﻿using Dapper;
using DapperFundamentals.Entities;
using Dapperundamentals.Abstractions;
using Microsoft.Data.SqlClient;

namespace EntityFundamentals.Repositories
{
    public class SqlProductsRepository : IRepository<Product>
    {
        private readonly string _connectionString;

        public SqlProductsRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Insert(Product product)
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "INSERT INTO Products (Name, Description, Weight, Height, Width, Length) OUTPUT INSERTED.Id VALUES (@Name, @Description, @Weight, @Height, @Width, @Length);";

            try
            {
                int? userId = dbContext.QuerySingle<int>(query, product);

                return userId is null
                    ? throw new Exception()
                    : userId.Value;
            }
            catch (Exception ex)
            {
                throw new Exception($"The insert of product wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }

        public Product Get(int productId)
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "SELECT Id, Name, Description, Weight, Height, Width, Length FROM Products WHERE Id = @Id";
            var order = dbContext.Query<Product>(query, new { Id = productId }).FirstOrDefault();

            return order is null
                ? throw new Exception($"The order with such ID doesn't exist.")
                : order;
        }

        public IReadOnlyList<Product> GetProductsList()
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "SELECT (Id, Name, Description, Weight, Height, Width, Length) FROM Products";
            var orders = dbContext.Query<Product>(query).ToList();

            return orders.Count == 0
                ? throw new Exception($"There is no any products.")
                : orders;
        }

        public void Update(Product product)
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "UPDATE Products SET Name = @Name, Description = @Description, Weight = @Weight, Height = @Height, Width = @Width, Length = @Length WHERE Id = @Id";

            try
            {
                dbContext.Execute(query, new Product 
                { 
                    Id = product.Id,
                    Name = product.Name,
                    Description = product.Description,
                    Weight = product.Weight,
                    Height = product.Height,
                    Width = product.Width,
                    Length = product.Length 
                });
            }
            catch (Exception ex)
            {
                throw new Exception($"The update of order wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }

        public void Delete(int productId)
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "DELETE FROM Products WHERE Id = @Id";

            try
            {
                dbContext.Execute(query, new { Id = productId });
            }
            catch (Exception ex)
            {
                throw new Exception($"The deletion of order by ID wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }
    }
}
