﻿using Dapper;
using DapperFundamentals;
using DapperFundamentals.Entities;
using DapperFundamentals.Models;
using Dapperundamentals.Abstractions;
using Microsoft.Data.SqlClient;

namespace EntityFundamentals.Repositories
{
    public class SqlOrdersRepository : IRepository<Order>
    {
        private readonly string _connectionString;

        public SqlOrdersRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Insert(Order order)
        {
            if (!Enum.IsDefined(typeof(Statuses), order.Status))
            {
                throw new ArgumentException("Specified status is not valid.");
            }

            using var dbContext = new SqlConnection(_connectionString);
            var query = "INSERT INTO Orders (Status, CreatedDate, UpdatedDate, ProductId) OUTPUT INSERTED.Id VALUES (@Status, @CreatedDate, @UpdatedDate, @ProductId);";
            
            order.CreatedDate = DateTime.Now;
            order.UpdatedDate = DateTime.Now;

            try
            {
                int? orderId = dbContext.QuerySingle<int>(query, order);

                return orderId is null
                    ? throw new Exception()
                    : orderId.Value;
            }
            catch (Exception ex)
            {
                throw new Exception($"The insert of order wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }

        public Order Get(int orderId)
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "SELECT Id, Status, CreatedDate, UpdatedDate, ProductId FROM Orders WHERE Id = @Id;";
            var order = dbContext.Query<Order>(query, new { Id = orderId }).FirstOrDefault();

            return order is null
                ? throw new Exception($"The order with such ID doesn't exist.")
                : order;
        }

        public void Update(Order order)
        {
            if (!Enum.IsDefined(typeof(Statuses), order.Status))
            {
                throw new ArgumentException("Specified status is not valid.");
            }

            using var dbContext = new SqlConnection(_connectionString);
            var query = "UPDATE Orders SET Status = @Status, UpdatedDate = @UpdatedDate, ProductId = @ProductId WHERE Id = @Id";

            try
            {
                dbContext.Execute(query, new Order 
                { 
                    Id = order.Id,
                    Status = order.Status,
                    UpdatedDate = DateTime.Now,
                    ProductId = order.ProductId
                });
            }
            catch (Exception ex)
            {
                throw new Exception($"The update of order wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }

        public void Delete(int orderId)
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "DELETE FROM Orders WHERE Id = @Id";

            try
            {
                dbContext.Execute(query, new { Id = orderId });
            }
            catch (Exception ex)
            {
                throw new Exception($"The deletion of order by ID wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }

        public void DeleteOrderByProductId(int productId)
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "DELETE FROM Orders WHERE ProductId = @Id";

            try
            {
                dbContext.Execute(query, new { Id = productId });
            }
            catch (Exception ex)
            {
                throw new Exception($"The deletion of order by ProductId wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }

        public IReadOnlyList<Order> SearchOrdersByCriteria(SearchOrderModel searchOrderModel)
        {
            using var dbContext = new SqlConnection(_connectionString);
            var query = "EXEC SearchOrders @Month, @Year, @Status, @ProductName";

            try
            {
                var orders = dbContext.Query<Order>(query, new
                {
                    Month = searchOrderModel.Month,
                    Year = searchOrderModel.Year,
                    Status = searchOrderModel.Status,
                    ProductName = searchOrderModel.ProductName
                }).ToList();

                return orders.Count == 0
                    ? throw new Exception()
                    : orders;
            }
            catch (Exception ex)
            {
                throw new Exception($"The orders with such criterias don't exist.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }

        public void DeleteOrdersByCriteria(SearchOrderModel searchOrderModel)
        {

            using var dbContext = new SqlConnection(_connectionString);
            var query = "EXEC DeleteOrders @Month, @Year, @Status, @ProductName";

            try
            {
                dbContext.Execute(query, new 
                { 
                    Month = searchOrderModel.Month,
                    Year = searchOrderModel.Year,
                    Status = searchOrderModel.Status,
                    ProductName = searchOrderModel.ProductName 
                });
            }
            catch (Exception ex)
            {
                throw new Exception($"The deletion of order by criteria wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Close();
            }
        }
    }
}
