using DapperFundamentals.Entities;
using EntityFundamentals.Repositories;
using NUnit.Framework;
using System;

namespace DapperFundamentals.IntegrationTests
{
    public class SqlOrdersRepositoryTests
    {
        private const string ConnectionString = "Data Source=localhost;Initial Catalog=EStore;Integrated Security=True;TrustServerCertificate=True";
        private readonly SqlOrdersRepository _ordersRepository;

        public SqlOrdersRepositoryTests()
        {
            _ordersRepository = new SqlOrdersRepository(ConnectionString);
        }

        [Test]
        public void Insert_Order_ReturnOrderId()
        {
            // Arrange
            var order = new Order
            {
                Status = Statuses.Arrived.ToString(),
                ProductId = 1
            };

            // Act
            var id = _ordersRepository.Insert(order);

            // Assert
            Assert.Greater(id, 0);
        }

        [Test]
        public void Get_OrderById_ReturnOrder()
        {
            var order = new Order
            {
                Id = 1,
                Status = "NotStarted",
                ProductId = 1,
            };

            var result = _ordersRepository.Get(order.Id);

            Assert.AreEqual(order.Id, result.Id);
            Assert.AreEqual(order.Status, result.Status);
            Assert.AreEqual(order.ProductId, result.ProductId);
        }

        [Test]
        public void Update_Order_OrderIsChanged()
        {
            var changedOrder = new Order
            {
                Id = 1,
                Status = Statuses.Done.ToString(),
                ProductId = 1
            };

            _ordersRepository.Update(changedOrder);
            var resultOrder = _ordersRepository.Get(changedOrder.Id);

            Assert.AreEqual(changedOrder.ProductId, resultOrder.ProductId);
            Assert.AreEqual(changedOrder.Status, resultOrder.Status);
            Assert.AreEqual(changedOrder.ProductId, resultOrder.ProductId);
        }

        [Test]
        public void Delet_OrderById_OrderIsDeleted()
        {
            var orderId = 1010;

            _ordersRepository.Delete(orderId);

            Assert.Throws<Exception>(() => _ordersRepository.Get(orderId));
        }

        [Test]
        public void DeleteOrderByProductId_OrdersAreDeleted()
        {
            var productId = 4;
            var deletedOrderId = 5;

            _ordersRepository.DeleteOrderByProductId(productId);

            Assert.Throws<Exception>(() => _ordersRepository.Get(deletedOrderId));
        }
    }
}