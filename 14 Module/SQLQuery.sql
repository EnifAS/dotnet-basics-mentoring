CREATE DATABASE EStore

GO

USE EStore

GO

CREATE TABLE Products
(
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(50) NOT NULL,
	Description VARCHAR(200),
	Weight DECIMAL,
	Height DECIMAL,
	Width DECIMAL,
	Length DECIMAL,
);

GO

CREATE TABLE Orders
(
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Status VARCHAR(10),
	CreatedDate DATETIME,
	UpdatedDate DATETIME,
	ProductId INT FOREIGN KEY REFERENCES Products(Id)
);

GO

INSERT INTO Products (Name, Description, Weight, Height, Width, Length)
VALUES ('Book', 'Book description', 500, 150, 200, 100)

GO

INSERT INTO Products (Name, Description, Weight, Height, Width, Length)
VALUES ('Book', 'Book description', 500, 150, 200, 100)

GO

INSERT INTO Products (Name, Description, Weight, Height, Width, Length)
VALUES ('Laptop', 'Laptop description', 1200, 120, 220, 110)

GO

INSERT INTO Products (Name, Description, Weight, Height, Width, Length)
VALUES ('Cookie', 'Cookie description', 300, 1150, 2200, 10)

GO

INSERT INTO Products (Name, Description, Weight, Height, Width, Length)
VALUES ('Refrigerator', 'Refrigerator description', 5000, 100500, 2100, 10110)

GO

INSERT INTO Orders (Status, CreatedDate, UpdatedDate, ProductId)
VALUES ('NotStarted', '20120618 10:34:09 AM', '20120618 10:34:09 AM', 1)

GO

INSERT INTO Orders (Status, CreatedDate, UpdatedDate, ProductId)
VALUES ('Arrived', '20200118 10:34:09 AM', '20200118 10:34:09 AM', 1)

GO

INSERT INTO Orders (Status, CreatedDate, UpdatedDate, ProductId)
VALUES ('Done', '20220315 10:34:09 AM', '20220315 10:34:09 AM', 2)

GO

INSERT INTO Orders (Status, CreatedDate, UpdatedDate, ProductId)
VALUES ('Done', '20220101 10:34:09 AM', '20220101 10:34:09 AM', 3)


GO

INSERT INTO Orders (Status, CreatedDate, UpdatedDate, ProductId)
VALUES ('Cancelled', '20220301 10:34:09 AM', '20220301 10:34:09 AM', 4)


GO

INSERT INTO Orders (Status, CreatedDate, UpdatedDate, ProductId)
VALUES ('Cancelled', '20220301 10:34:09 AM', '20220301 10:34:09 AM', 3)

GO

CREATE PROCEDURE SearchOrders
@Month int = NULL,
@Year int = NULL,
@Status varchar(10) = NULL,
@ProductName varchar(50) = NULL
AS
BEGIN

DECLARE @ProductId INT = NULL

IF (@ProductName IS NOT NULL)
BEGIN
	SELECT @ProductId = (SELECT Id FROM Products WHERE Name = @ProductName)
END

SELECT Id, Status, CreatedDate, UpdatedDate, ProductId
FROM Orders
WHERE (@Status IS NULL OR Status = @Status)
AND (@Month IS NULL OR MONTH(CreatedDate) = @Month)
AND (@Year IS NULL OR YEAR(CreatedDate) = @Year)
AND (@ProductName IS NULL OR ProductId = @ProductId)

RETURN
END

GO

CREATE PROCEDURE DeleteOrders
@Month int = NULL,
@Year int = NULL,
@Status varchar(10) = NULL,
@ProductName varchar(50) = NULL
AS
BEGIN

DECLARE @ProductId INT = NULL

IF (@ProductName IS NOT NULL)
BEGIN
	SELECT @ProductId = (SELECT Id FROM Products WHERE Name = @ProductName)
END

BEGIN TRAN

DELETE FROM Orders
WHERE (@Status IS NULL OR Status = @Status)
AND (@Month IS NULL OR MONTH(CreatedDate) = @Month)
AND (@Year IS NULL OR YEAR(CreatedDate) = @Year)
AND (@ProductName IS NULL OR ProductId = @ProductId)

COMMIT
END

GO