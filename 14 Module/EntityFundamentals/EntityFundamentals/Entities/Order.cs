﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFundamentals.Entities
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Indicate the status of order.")]
        [MaxLength(10, ErrorMessage = "Status name cannot be longer than 50 charecters.")]
        public string Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        [ForeignKey("Product")]
        [Required]
        public int ProductId { get; set; }
    }
}
