﻿namespace EntityFundamentals.Abstractions
{
    public interface IRepository<T>
    {
        int Insert(T entity);

        void Update(T entity);

        void Delete(int entityId);

        T Get(int entityId);
    }
}
