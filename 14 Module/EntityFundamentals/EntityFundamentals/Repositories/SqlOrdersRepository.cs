﻿using EntityFundamentals.Abstractions;
using EntityFundamentals.Entities;
using EntityFundamentals.Models;
using Microsoft.EntityFrameworkCore;

namespace EntityFundamentals.Repositories
{
    public class SqlOrdersRepository : IRepository<Order>
    {
        private readonly string _connectionString;

        public SqlOrdersRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Insert(Order order)
        {
            if (!Enum.IsDefined(typeof(Statuses), order.Status))
            {
                throw new ArgumentException("Specified status is not valid.");
            }

            using var dbContext = new ApplicationDbContext(_connectionString);

            var orderForInsert = new Order
            {
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                ProductId = order.ProductId,
                Status = order.Status,
            };

            try
            {
                dbContext.Orders.Add(orderForInsert);
                dbContext.SaveChanges();
                return orderForInsert.Id;
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException($"The insert of order wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Database.CloseConnection();
            }
        }

        public Order Get(int orderId)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);
            var order = dbContext.Orders.FirstOrDefault(order => order.Id == orderId);

            return order ?? throw new Exception($"The order with such ID doesn't exist.");
        }

        public void Update(Order order)
        {
            if (!Enum.IsDefined(typeof(Statuses), order.Status))
            {
                throw new ArgumentException("Specified status is not valid.");
            }

            using var dbContext = new ApplicationDbContext(_connectionString);
            order.UpdatedDate = DateTime.Now;

            try
            {
                dbContext.Orders.Update(order);
                dbContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException($"The update of order wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Database.CloseConnection();
            }
        }

        public void Delete(int orderId)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);

            try
            {
                var order = dbContext.Orders.Find(orderId);

                if (order is null)
                {
                    throw new Exception($"The order with such ID doesn't exist.");
                }

                dbContext.Orders.Remove(order);
                dbContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException($"The deletion of order by ID wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Database.CloseConnection();
            }
        }

        public void DeleteOrderByProductId(int productId)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);

            try
            {
                var orders = dbContext.Orders.Where(order => order.ProductId == productId);

                if (!orders.Any())
                {
                    throw new Exception($"The orders with such ProductId don't exist.");
                }

                dbContext.Orders.RemoveRange(orders);
                dbContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException($"The deletion of order by ID wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Database.CloseConnection();
            }
        }

        public IReadOnlyList<Order> SearchOrdersByCriteria(SearchOrderModel searchOrderModel)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);

            var orders = dbContext.Orders.FromSqlRaw(
                $"EXEC SearchOrders @Month, @Year, @Status, @ProductName",
                searchOrderModel.Month,
                searchOrderModel.Year,
                searchOrderModel.Status,
                searchOrderModel.ProductName);

            if (!orders.Any())
            {
                throw new Exception($"The orders with such criterias don't exist.");
            }

            return orders.ToList();
        }

        public void DeleteOrdersByCriteria(SearchOrderModel searchOrderModel)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);

            var orders = dbContext.Orders.FromSqlRaw(
                $"EXEC DeleteOrders @Month, @Year, @Status, @ProductName",
                searchOrderModel.Month,
                searchOrderModel.Year,
                searchOrderModel.Status,
                searchOrderModel.ProductName)
                .ToList();

            if (!orders.Any())
            {
                throw new Exception($"The orders with such criterias don't exist.");
            }

            try
            {
                dbContext.Orders.RemoveRange(orders);
                dbContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException($"The deletion of order by critaria wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Database.CloseConnection();
            }
        }
    }
}
