﻿using EntityFundamentals.Abstractions;
using EntityFundamentals.Entities;
using Microsoft.EntityFrameworkCore;

namespace EntityFundamentals.Repositories
{
    public class SqlProductsRepository : IRepository<Product>
    {
        private readonly string _connectionString;

        public SqlProductsRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Insert(Product product)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);

            try
            {
                dbContext.Products.Add(product);
                dbContext.SaveChanges();
                return product.Id;
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException($"The insert of product wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Database.CloseConnection();
            }
        }

        public Product Get(int productId)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);
            var product = dbContext.Products.FirstOrDefault(product => product.Id == productId);

            return product ?? throw new Exception($"The product with such ID doesn't exist.");
        }

        public IReadOnlyList<Product> GetProductsList()
        {
            using var dbContext = new ApplicationDbContext(_connectionString);
            var products = dbContext.Products.Select(product => product).ToList();

            return products.Count == 0 
                ? throw new Exception($"The products with such ID doesn't exist.")
                : products;
        }

        public void Update(Product product)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);

            try
            {
                dbContext.Products.Update(product);
                dbContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException($"The update of product wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Database.CloseConnection();
            }
        }

        public void Delete(int productId)
        {
            using var dbContext = new ApplicationDbContext(_connectionString);

            try
            {
                var product = dbContext.Products.Find(productId);

                if (product is null)
                {
                    throw new Exception($"The product with such ID doesn't exist.");
                }

                dbContext.Products.Remove(product);
                dbContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException($"The deletion of product by ID wasn't successfull.", ex);
            }
            finally
            {
                dbContext.Database.CloseConnection();
            }
        }
    }
}
