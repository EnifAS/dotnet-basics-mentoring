﻿using EntityFundamentals.Entities;
using EntityFundamentals.Repositories;
using NUnit.Framework;
using System;

namespace EntityFundamentals.IntegrationTests
{
    public class SqlProductsRepositoryTests
    {
        private const string ConnectionString = "Data Source=localhost;Initial Catalog=EStore;Integrated Security=True;TrustServerCertificate=True";
        private readonly SqlProductsRepository _productsRepository;

        public SqlProductsRepositoryTests()
        {
            _productsRepository = new SqlProductsRepository(ConnectionString);
        }

        [Test]
        public void Insert_Product_ReturnProductId()
        {
            // Arrange
            var product = new Product
            {
                Name = "Teapot",
                Description = "Teapot description",
                Weight = 1.43M,
                Height = 20,
                Length = 10,
                Width = 10
            };

            // Act
            var id = _productsRepository.Insert(product);

            // Assert
            Assert.Greater(id, 0);
        }

        [Test]
        public void Get_ProductById_ReturnProduct()
        {
            var product = new Product
            {
                Id = 1,
                Name = "Book",
                Description = "Book description",
                Weight = 500,
                Height = 150,
                Width = 200,
                Length = 100
            };

            var result = _productsRepository.Get(product.Id);

            Assert.AreEqual(product.Id, result.Id);
            Assert.AreEqual(product.Name, result.Name);
            Assert.AreEqual(product.Description, product.Description);
            Assert.AreEqual(product.Weight, result.Weight);
            Assert.AreEqual(product.Height, result.Height);
            Assert.AreEqual(product.Width, result.Width);
            Assert.AreEqual(product.Length, result.Length);
        }

        [Test]
        public void Update_Product_ProductIsChanged()
        {
            var changedProduct = new Product
            {
                Id = 1003,
                Name = "Book",
                Description = "Book description1",
                Weight = 500,
                Height = 150,
                Width = 200,
                Length = 100
            };

            _productsRepository.Update(changedProduct);
            var resultProduct = _productsRepository.Get(changedProduct.Id);

            Assert.AreEqual(changedProduct.Id, resultProduct.Id);
            Assert.AreEqual(changedProduct.Name, resultProduct.Name);
            Assert.AreEqual(changedProduct.Description, resultProduct.Description);
            Assert.AreEqual(changedProduct.Weight, resultProduct.Weight);
            Assert.AreEqual(changedProduct.Height, resultProduct.Height);
            Assert.AreEqual(changedProduct.Width, resultProduct.Width);
            Assert.AreEqual(changedProduct.Length, resultProduct.Length);
        }

        [Test]
        public void Delet_ProductById_ProductIsDeleted()
        {
            var productId = 1003;

            _productsRepository.Delete(productId);

            Assert.Throws<Exception>(() => _productsRepository.Get(productId));
        }
    }
}
