﻿using System.Collections.Generic;

namespace Models
{
    public class FilesInformation
    {
        public static List<Department> Departments = new List<Department>
        {
            new Department
            {
                DepartmentName = "Marketing",

                Employees = new List<Employee>
                {
                    new Employee { EmpoyeeName = "Nazar Gohar" },
                    new Employee { EmpoyeeName = "Zhirayr Stanek" },
                    new Employee { EmpoyeeName = "Ari Brzezicki" },
                    new Employee { EmpoyeeName = "Avag Matevosyan" },
                    new Employee { }
                }
            },
            
            new Department
            {
                DepartmentName = "Finance",

                Employees = new List<Employee>
                {
                    new Employee { EmpoyeeName = "Maxim Winogrodzki" },
                    new Employee { EmpoyeeName = "Ermolai Wronski" },
                    new Employee { EmpoyeeName = "Anika Kijek" },
                    new Employee { EmpoyeeName = "Fedora Sitko" }
                }
            }
        };
    }
}
