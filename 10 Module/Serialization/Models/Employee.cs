﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace Models
{
    [Serializable]
    public class Employee : ICloneable
    {
        [XmlElement(ElementName = "EmployeeFullName")]
        [JsonIgnore( Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string EmpoyeeName { get; set; }

        public object Clone()
        {
            using var memoryStream = new MemoryStream();

            try
            {
                if (this.GetType().IsSerializable)
                {
                    var formatter = new BinaryFormatter();

                    formatter.Serialize(memoryStream, this);

                    memoryStream.Position = 0;

                    return formatter.Deserialize(memoryStream);
                }
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("The Deep Cloning is failed. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                memoryStream.Close();
            }

            return null;
        }
    }
}
