﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Models
{
    [Serializable]
    public class Department : ICloneable
    {
        public string DepartmentName { get; set; }

        public List<Employee> Employees { get; set; }

        public object Clone()
        {
            using var memoryStream = new MemoryStream();

            try
            {
                if (this.GetType().IsSerializable)
                {
                    var formatter = new BinaryFormatter();

                    formatter.Serialize(memoryStream, this);

                    memoryStream.Position = 0;

                    return formatter.Deserialize(memoryStream);
                }
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("The Deep Cloning is failed. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                memoryStream.Close();
            }

            return null;
        }
    }
}
