﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CustomSerializer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var fileName = "Pet data.myData";

            var pet = new Pet
            {
                AnimalType = "dog",
                Age = 3,
                Name = "Bean",
                FullyVaccinated = true
            };

            var formatter = new BinaryFormatter();

            SerializeItem(pet, fileName, formatter);
            DeserializeItem<Pet>(fileName, formatter);

            Console.WriteLine(pet.AnimalType);
            Console.WriteLine(pet.Age);
            Console.WriteLine(pet.Name);
            Console.WriteLine(pet.FullyVaccinated);
        }

        public static void SerializeItem<T>(T data, string fileName, IFormatter formatter)
        {
            using var fileStream = new FileStream(fileName, FileMode.Create);

            try
            {
                formatter.Serialize(fileStream, data);
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Failed to serialize. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                fileStream.Close();
            }
        }

        public static T DeserializeItem<T>(string fileName, IFormatter formatter)
        {
            using var fileStream = new FileStream(fileName, FileMode.Open);

            try
            {
                return (T)formatter.Deserialize(fileStream);
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Failed to serialize. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                fileStream.Close();
            }

            return default;
        }
    }
}
