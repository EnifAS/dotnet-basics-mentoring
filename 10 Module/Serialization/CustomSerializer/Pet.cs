﻿using System;
using System.Runtime.Serialization;

namespace CustomSerializer
{
    [Serializable]
    public class Pet : ISerializable
    {
        public string AnimalType { get; set; }

        public int Age { get; set; }

        public string Name { get; set; }

        public bool FullyVaccinated { get; set; }

        public Pet() { }

        public Pet(SerializationInfo info, StreamingContext context)
        {
            AnimalType = info.GetString("animalType");
            Age = info.GetInt32("age");
            Name = info.GetString("name");
            FullyVaccinated = info.GetBoolean("fullyVaccinated");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("animalType", AnimalType, typeof(string));
            info.AddValue("age", Age, typeof(int));
            info.AddValue("name", Name, typeof(string));
            info.AddValue("fullyVaccinated", FullyVaccinated, typeof(bool));
        }
    }
}