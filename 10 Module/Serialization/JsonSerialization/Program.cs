﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Threading.Tasks;

namespace JsonSerialization
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var fileName = "JSON serialization.json";

            await Serialize(FilesInformation.Departments, fileName);

            var deserializedFile = await Deserialize<List<Department>>(fileName);
        }

        public static async Task Serialize<T>(T data, string fileName)
        {
            using var fileStream = new FileStream(fileName, FileMode.Create);

            try
            {
                await JsonSerializer.SerializeAsync(fileStream, data);
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Failed to serialize. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                fileStream.Close();
            }
        }

        public static async Task<T> Deserialize<T>(string fileName)
        {
            using var fileStream = new FileStream(fileName, FileMode.Open);

            try
            {
                return await JsonSerializer.DeserializeAsync<T>(fileStream);
            }
            catch (SerializationException ex)
            {

                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Failed to serialize. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                fileStream.Close();
            }

            return default;
        }
    }
}
