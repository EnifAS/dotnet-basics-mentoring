﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Models;

namespace BinarySerialization
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var fileName = "Binary serialization.txt";

            Serialize(FilesInformation.Departments, fileName);

            var deserializedFile = Deserialize<List<Department>>(fileName);
        }

        public static void Serialize<T>(T data, string fileName)
        {
            using var fileStream = new FileStream(fileName, FileMode.Create);

            var formatter = new BinaryFormatter();

            try
            {
                formatter.Serialize(fileStream, data);
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Failed to serialize. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                fileStream.Close();
            }
        }

        public static T Deserialize<T>(string fileName)
        {
            using var fileStream = new FileStream(fileName, FileMode.Open);

            try
            {
                var formatter = new BinaryFormatter();

                return (T)formatter.Deserialize(fileStream);
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Failed to serialize. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally 
            { 
                fileStream.Close();
            }

            return default;
        }
    }
}
