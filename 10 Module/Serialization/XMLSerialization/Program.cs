﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace XMLSerialization
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var fileName = "XML serialization.xml";

            Selialize(FilesInformation.Departments, fileName);

            var deserializedFile = Deserialize<List<Department>>(fileName);
        }

        public static void Selialize<T>(T data, string fileName)
        {
            var serializer = new XmlSerializer(typeof(T));

            using var streamWriter = new StreamWriter(fileName);

            try
            {
                serializer.Serialize(streamWriter, data);
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Failed to serialize. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                streamWriter.Close();
            }
        }

        public static T Deserialize<T>(string fileName)
        {
            var serializer = new XmlSerializer(typeof(T));

            var fileStream = new FileStream(fileName, FileMode.Open);

            try
            {
                return (T)serializer.Deserialize(fileStream);
            }
            catch (SerializationException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Failed to serialize. Reason: " + ex.Message);

                Console.ResetColor();
            }
            finally
            {
                fileStream.Close();
            }

            return default;
        }
    }
}
