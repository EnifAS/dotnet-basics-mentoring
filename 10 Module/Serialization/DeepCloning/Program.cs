﻿using System;
using Models;
using System.Linq;
using System.Collections.Generic;

namespace DeepCloning
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var departmentsList = new List<Department>();
            
            FilesInformation.Departments.ForEach(
                department => departmentsList.Add(
                    (Department)department.Clone()));

            departmentsList.First().Employees = new List<Employee>
            {
                new Employee { EmpoyeeName = "Kobe Lee" },
                new Employee { EmpoyeeName = "Zavier Morgan" },
                new Employee { EmpoyeeName = "Ibrahim Jones" },
                new Employee { EmpoyeeName = "Vanessa Coleman" },
                new Employee { EmpoyeeName = "Elena Adams" },
            };

            FilesInformation.Departments
                .First()
                .Employees.ForEach(
                employee => Console.WriteLine(employee.EmpoyeeName));

            Console.WriteLine("----------------------");

            departmentsList
                .First()
                .Employees.ForEach(
                employee => Console.WriteLine(employee.EmpoyeeName));
        }
    }
}
