using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Email;
using System;
using System.Net;

namespace BrainstormSessions
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.File("..\\..\\serilog.txt")
                .WriteTo.Email(new EmailConnectionInfo
                {
                    FromEmail = "",          // Email sender
                    ToEmail = "",            // Email receiver
                    MailServer = "",         // Sender's mail server 
                    NetworkCredentials = new NetworkCredential
                    {
                        UserName = "",       // Email sender's username (mail address)
                        Password = ""        // Password for this mail
                    },
                    EnableSsl = true,
                    Port = 465,
                    EmailSubject = "Brainstorm Session Error"
                }, restrictedToMinimumLevel: LogEventLevel.Information, batchPostingLimit: 10)
                .WriteTo.Console()
                .CreateLogger();

            try
            {
                Log.Information("Starting web host");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
