﻿namespace HelloLibrary
{
    public static class HelloHelper
    {
        public static string SayHello(string name) => $"Hello {name}!";
    }
}
