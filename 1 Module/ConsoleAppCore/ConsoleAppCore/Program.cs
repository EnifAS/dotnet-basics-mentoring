﻿using System;
using HelloLibrary;

namespace ConsoleAppCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please, enter your name.");

            var greeting = HelloHelper.SayHello(Console.ReadLine());

            Console.Clear();

            Console.WriteLine(greeting);
        }
    }
}
